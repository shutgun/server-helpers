# Server Helpers
I manage a few Linux driven webservers. Most of them run Cent OS with Plesk. This repo collects a few helper scripts, I use.

## dkim-record.sh
Generate the DNS TXT record for DKIM from the Plesk domainkey.

usage: `dkim-record.sh <domain>`