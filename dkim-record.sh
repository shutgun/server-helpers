#!/bin/bash -e

# Check for domainname as parameter
if [[ $# -eq 0 ]]; then
	>&2 echo "Please give a domainname."
	exit 1
fi

# Output DKIM record if private key is found
if [[ -f /etc/domainkeys/$1/default ]]; then
	PUBKEY=$(openssl rsa -in /etc/domainkeys/$1/default -pubout 2>/dev/null | grep -v -- ----- | tr -d '\n')
	echo -e "default._domainkey\tv=DKIM1;k=rsa;p=$PUBKEY;"
else
	>&2 echo "Domain \"$1\" does not exist on this server."
fi
